[1] "Structure and Interpretation of Computer Programs", Abelson, Jay Sussman, 2nd edition, Ed. MIT, 1996, p.6.  https://web.mit.edu/alexmv/6.037/sicp.pdf

[2] Le modèle OSI (*Open Systems Interconnection model*) est une représentation normalisée du traitement d'un message reçu par un ordinateur. Concrètement le message est routé à travers 7 types d'applications, depuis son arrivée (*Layer 1* réception du signal électrique) jusqu'à sa destination (l'espace utilisateur).

[3] Le Garbage Collector est un mécanisme de récupération automatique de mémoire inutilisés au sein d'une application. GC était une innovation du language LISP.

[4] Cette famille d'adresses permet la communication entre des processus qui s'exécutent sur le même système ou sur des systèmes différents. Les adresses des sockets AF_INET sont de type adresse IP et numéro de port.
