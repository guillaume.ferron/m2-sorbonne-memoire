
**
 Mémoire de Master2 pour le master Métiers Informatiques et Maitrise d'Ouvrage (MIMO)
# Directeur de mémoire : Rebecca Denecker
## Pour : UFR09, Paris 1 La Sorbonne
## Auteur : Guillaume Ferron
**

*Soutenu le 6 septembre 2019*


#### BLOC 1.md

En 1985 fût publié un livre considéré comme légendaire par de nombreux hackers. Baptisé "Structure et interprétation des programmes informatiques" (Structure and Interpretation of Computer Programs or SICP) l’ouvrage prend la forme d’un manuel explicitant, souvent de manière philosophique, les rouages internes du langage Lisp créé par John McCarthy vers la fin des années 50.

Surnommé "Livre du magicien" (Wizard Book) l’ouvrage, écrit par Hal Abelson et Gerald Jay Sussman, a éduqué plusieurs générations d'informaticiens. Il présente une méthode particulièrement intéressante pour s’approprier Lisp ainsi que tout autre langage. Principe : tout langage peut
-être rapporté à 3 éléments simple, martèle par ses auteurs, à savoir des primitifs, des combinaisons et des moyens d’abstractions.

#### BLOC 2.md

**Un primitif**, selon les auteurs, correspond à la plus petite entité logique qu’un langage peut manipuler. Il s’agit par exemple des types fournis avec le langage, comme `int`, `float` ou encore `char`. Il peut également s’agir de fonctions. Soit celles implémentées nativement, très proche du noyau et donc du langage machine. Soit celles dites `lambda`, très haute dans le modèle OSI[2] qui se voient associées un contexte temporaire avant de s’évanouir dans le collecteur de déchets (*Garbage Collector*[3]). Souvent *Immutables*, autrement dit protégé contre les modifications, ces primo
-éléments peuvent par ailleurs être étendue à l’infini via un système de classe et d’héritage.

#### BLOC 3.md

**Les combinaisons**, de leur coté, représente l’adjonction d’un opérateur à un opérande. Lequel peut
-être un primitif, comme lorsqu’un `int` se voit additionné à un `int` ou qu’un char est rattaché à autre `char`. Il peut également s’agir d’autre combinaison, auquel cas la fonction assurant les combinaisons est récursive et se rappelle elle
-même. 

Concrètement les opérandes sont le moyen pour les opérateurs primitifs d’interagir les uns avec les autres. Fabriquant, dès lors, un terrain propice sur lequel peut fleurir des combinaisons originales (sauf en cas de combinaisons soient interdites, à l’instar de la division par zéro).

#### BLOC 4.md

**Les  moyens d'abstraction.** Dernier élément du triptyque élaboré par les deux professeurs du MIT : les éléments fournis en standard et visant à encapsuler des instructions dans un bloc de code exécutable moyennant l’usage d’un simple mot.

Parmi les moyens d’abstraction, citons la Procédure, une portion de code représentant un sous
-programme qui, plutôt que d’exposer son algorithme, met l’accent sur le type d’argument qu’elle accepte en entrée.

Selon les auteurs, ces seuls 3 éléments sont suffisants pour écrire un langage informatique. Peuvent
-ils être suffisant pour concevoir un système d’information minimal ?

#### BLOC 5.md

C’est l’hypothèse que nous décidons de formuler.

En effet, tout comme LISP, un système d’information est un ensemble organisé de procédures concourant dans le but d’exécuter une tâche.  Tout comme LISP, un SI s’attache à collecter, stocker, traiter et distribuer de l'information. La seule différence, technique, finalement, c’est qu’un système d’information communique généralement via des sockets AF_INET[4], comprenant TCP/IP. LISP – comme tout langage UNIX – utilise des sockets AF_UNIX.

Nous croyons qu’il existe un algorithme capable de construire le système d’information minimal que nous recherchons pour la presse, à partir des 3 outils de base qui ont servi à construire LISP. À savoir les primitifs, les procédures et des moyens d'abstractions (qui pour nous seront des classes).

#### BLOC 6.md

Lister les primitifs du journalisme. Première instruction donnée par la méthode, dresser la liste exhaustive des fonctions et objets contenu dans le système cible, soit l’agence de presse de test.

Les primitifs les plus utilisés sont les suivants.

Rôles primitifs

- journaliste

- rédacteur en chef

- directeur de publication


Objets primitifs

- Articles de presse

- Communiqués

- Événements de presse

- Événements publics

- Propositions de sujet

- Factures

- Piges

- Avis d’enquêtes

- Appels à témoins

- Verbatims/Interviews

- Photographie (légendée attribuée)

- Enregistrements video (légendée attribuée)

- Enregistrements audio (légendée attribuée)

- Bulletins de prise de commande

- Conférence de rédaction


Procédures :

- écrire

- apprendre

- téléphoner

- écrire un email

- écrire un sms

- se déplacer

- poser une question

- relire

- demander une relecture

- valider une relecture

- packager

- livrer

- rester en veille

- participer







#### BLOC 7.md

Reste à attribuer une catégorie (sujet, verbe ou objet) à chacun des primitifs récoltés.

Rôles primitifs

- SUJET: journaliste

- SUJET: rédacteur en chef

- SUJET: directeur de publication


Objets primitifs

- COMPLEMENT : Articles de presse

- COMPLEMENT : Communiqués

- COMPLEMENT : Événements de presse

- COMPLEMENT : Événements publics

- COMPLEMENT :  Propositions de sujet

- COMPLEMENT : Factures

- COMPLEMENT : Piges

- COMPLEMENT : Avis d’enquêtes

- COMPLEMENT : Appels à témoins

- COMPLEMENT : Verbatims/Interviews

- COMPLEMENT : Photographie (légendée attribuée)

- COMPLEMENT : Enregistrements video (légendée attribuée)

- COMPLEMENT : Enregistrements audio (légendée attribuée)

- COMPLEMENT : Bulletins de prise de commande

- COMPLEMENT : Conférence de rédaction


Procédures :

- VERBE : écrire

- VERBE : apprendre

- VERBE : téléphoner

- VERBE : écrire un email

- VERBE : écrire un sms

- VERBE : se déplacer

- VERBE : poser une question

- VERBE : relire

- VERBE : demander une relecture

- VERBE : valider une relecture

- VERBE : packager

- VERBE : livrer

- VERBE : rester en veille

- VERBE : participer


#### BLOC 8.md

Puis à grouper ces primitifs par fréquence d'apparition dans la rédaction. Pour obtenir ce type de données, la meilleure solution sera la plus discrète.



#### BLOC 9.md


#### BLOC 10.md

 lister toutes les procédures de structure "sujet verbe complement"


#### BLOC 11.md

 exclure manuellement les entités non pertinentes


#### BLOC 12.md

 l'alternative des réseaux de neurones


#### BLOC 13.md

 definition : les classes


#### BLOC 14.md

 grouper les procédures dans des classes avec un réseau de neurone


#### BLOC 15.md

 extraire les 10 procédures les plus fréquentes parmi tous les groupes


#### BLOC 16.md

 produire les spécifications fonctionnelles


#### BLOC 17.md

 type de langage recherché


#### BLOC 18.md

 C la solution longue


#### BLOC 19.md

 Java la solution simple mais risquée


#### BLOC 20.md

 Python la solution fonctionnelle


#### BLOC 21.md

 Associer programmation Objet et programmation fonctionnelle


#### BLOC 22.md

 développer sous Linux/GNU


#### BLOC 23.md

 fuir les environnements intégrés


#### BLOC 24.md

 démarrer avec la plus petite installation possible


#### BLOC 25.md

 OPTIONNEL enregistrer automatiquement des copies de la machine + modifier rm et mv


#### BLOC 26.md

 compiler la dernière version stable de python


#### BLOC 27.md

 Commencer par les tests


#### BLOC 28.md

 Installer git et adopter les pratiques de l'Open
-Source


#### BLOC 29.md

 obtenir d'un rédacteur en chef une écoute attentive


#### BLOC 30.md

 communiquer avec un journaliste


#### BLOC 31.md

 communiquer avec un non
-informaticien


#### BLOC 32.md

 s'appuyer sur les journalistes amateur de code


#### BLOC 33.md

 usage du "non"


#### BLOC 34.md

 ici on ne peut pas former les équipes


#### BLOC 35.md

 produire des interfaces standards multicanales


#### BLOC 36.md

 fournir une authentification SSO


#### BLOC 37.md

 faire passer les connexions dans un VPN


#### BLOC 38.md

 élaboration du plan de continuité d'activité


#### BLOC 39.md

 élaboration du Plan de reprise d'activité


#### BLOC 40.md

 automatiser les tests de sécurité des applications


#### BLOC 41.md

 publication d'articles


#### BLOC 42.md

 vente d'articles


#### BLOC 43.md

 système de paiment


#### BLOC 44.md

 publication d'avis d'enquête


#### BLOC 45.md

 publication d'appels à témoignages


#### BLOC 46.md

 consultation de communiqués de presse


#### BLOC 47.md

 consultation des événements de presse


#### BLOC 48.md

 communication entre les journalistes


#### BLOC 49.md

 interviews video avec retranscription audio


#### BLOC 50.md

 open interview


#### BLOC 51.md

 blockchain des verbatims


#### BLOC 52.md

 classification automatique des emails


#### BLOC 53.md

 [experimentation : programmer pendant l'enquête]


#### BLOC 54.md

 bilan


#### BLOC 55.md

 theHarvester, pour obtenir des adresses emails


#### BLOC 56.md

 Maltego, pour tracker l'historique publique d'une personne sur la toile


#### BLOC 57.md

 Metasploit
-console, pour chercher des leaks sur les serveurs sensibles


#### BLOC 58.md

 limites légales et éthique journalistique


#### BLOC 59.md

 fonctionnement d'un ESB


#### BLOC 60.md

 les ESB du marché


#### BLOC 61.md

 [experimentation] la solution WSO2


#### BLOC 62.md

 Docker
-CE ou LXC/LXD


#### BLOC 63.md

 Kubernetike


#### BLOC 64.md

 Sharder ses bases de données


#### BLOC 65.md

 Connecter un datawharehouse


#### BLOC 66.md

 activer les mises à jour de sécurité de ses containers


#### BLOC 67.md

