##### version 2

# Mémoire de Master 2
## Métiers Informatiques te Maitrise d'ouvrage
## Département UFR09, La Sorbonne

- Soutenance : 6 septembre 2019


## How to use

```
sudo apt-get install pandoc libreoffice-writer
cd src && ./compile && ./export && libreoffice-writer current.md
```
