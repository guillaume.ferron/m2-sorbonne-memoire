# Mémoire : Prototypage d'un système d'information journalistique : minimal, maintenable et évolutif

## Introduction

Definition : 
- un système d'information
- un secteur critique

### A) Conception du système (MOA)
0. Présentation de la méthode (1986)
- origine de la méthode
- definition : les primitifs
- definition : les combinaisons
- definition : les  moyens d'abstraction
- détournement de la méthode

1. Lister les primitifs du système
- lister les primitifs du journalisme
- attribuer une catégorie [sujet|verbe|objet]
- grouper les primitifs par fréquence d'apparition

2. Determiner les combinaisons possibles
- lister toutes les procédures de structure "sujet verbe complement"
- exclure manuellement les entités non pertinentes
- l'alternative des réseaux de neurones

3. Proposer des moyens d'abstraction
- definition : les classes
- grouper les procédures dans des classes avec un réseau de neurone
- extraire les 10 procédures les plus fréquentes parmi tous les groupes
- produire les spécifications fonctionnelles

### B) Construction du système (MOE)

0. choisir un langage
- type de langage recherché
- C la solution longue
- Java la solution simple mais risquée
- Python la solution fonctionnelle
- Associer programmation Objet et programmation fonctionnelle

1. concevoir un environnement de dévelopement
- développer sous Linux/GNU
- fuir les environnements intégrés
- démarrer avec la plus petite installation possible
- OPTIONNEL enregistrer automatiquement des copies de la machine + modifier rm et mv
- compiler la dernière version stable de python

2. programmer en intégration continue
- Commencer par les tests
- Installer git et adopter les pratiques de l'Open-Source

3. Organiser les livraisons à l'écart des dates de bouclage et des dates d'événements
- obtenir d'un rédacteur en chef une écoute attentive
- communiquer avec un journaliste
- communiquer avec un non-informaticien
- s'appuyer sur les journalistes amateur de code
- usage du "non"

4. Différence avec la construction dun système d'information dans un contexte moins critique
- ici on ne peut pas former les équipes
- produire des interfaces standards multicanales
- fournir une authentification SSO
- faire passer les connexions dans un VPN

5. Sécurisation standard
- élaboration du plan de continuité d'activité
- élaboration du Plan de reprise d'activité
- automatiser les tests de sécurité des applications
		  
### C) Les services du système

0. Installer les services requis
- publication d'articles
- vente d'articles
- système de paiment
- publication d'avis d'enquête
- publication d'appels à témoignages
- consultation de communiqués de presse
- consultation des événements de presse
- communication entre les journalistes
- interviews video avec retranscription audio

1. Concevoir de nouveaux services
- open interview
- blockchain des verbatims
- classification automatique des emails
- [experimentation : programmer pendant l'enquête]
- bilan

2. S'appuyer sur des services existants (par exemple les outils de hacking)
- theHarvester, pour obtenir des adresses emails
- Maltego, pour tracker l'historique publique d'une personne sur la toile
- Metasploit-console, pour chercher des leaks sur les serveurs sensibles
- limites légales et éthique journalistique

3. Installer un ESB pour intégrer à la volée de nouveaux services dans le système d'information
- fonctionnement d'un ESB
- les ESB du marché
- [experimentation] la solution WSO2

4. Déployer des containers
- Docker-CE ou LXC/LXD
- Kubernetike
- Sharder ses bases de données
- Connecter un datawharehouse
- activer les mises à jour de sécurité de ses containers

## Conclusion
